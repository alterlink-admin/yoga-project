export const state = () => ({
    answers: []
})

export const getters = {
    getAnswersCount(state) {
        return state.answers.length
    }
}

export const mutations = {
    addAnswers(state,{ groupId, itemId, value, ordering }) {
        state.answers.push({
            groupId,
            itemId,
            value,
            ordering
        })
    },
    updatedAnswers(state, payload) {
        const {groupId, itemId, value, ordering} = payload
        let answer = state.answers.find(a => a.ordering === ordering)
        answer.value = value
    },
}

export const actions = {

    ADD_ANSWERS({ commit, state }, payload) {
        const { groupId, itemId, value, ordering } = payload
        let answer = state.answers.find(a => a.ordering === ordering)
        if (!answer) {
            commit("addAnswers", {
                groupId, itemId, value, ordering
            })
        } else if (answer.ordering === ordering) {
            commit("updatedAnswers", payload)
        }
    },

    SUBMIT_ANSWERS({state}) {
        console.log(state.answers.length)
    }
}
